import { createCanvas, registerFont, loadImage } from 'canvas';
const sqlite = require('sqlite');
const sqlite3 = require('sqlite3');
const SQL = require('sql-template-strings');

const fetchAndIncrementsHits = async (id, justChecking = false) => {
    if (id === '0demo0') {
        return 2137;
    }

    const db = await sqlite.open({
        filename: __dirname + '/../db.sqlite',
        driver: sqlite3.Database,
    });

    const result = await db.get(SQL`SELECT hits FROM counters WHERE id = ${id}`);

    if (justChecking) {
        return result ? result.hits : 0;
    }

    let hits = 1;
    if (result) {
        hits += result.hits;
        await db.get(SQL`UPDATE counters SET hits = ${hits} WHERE id = ${id}`);
    } else {
        await db.get(SQL`INSERT INTO counters(id, hits) VALUES (${id}, ${hits})`);
    }

    return hits;
}

const drawCounter = async (text, backgroundColour, textColour, textSize, textFont) => {
    try {
        registerFont(`server/fonts/${textFont}-Regular.ttf`, { family: textFont, weight: 'regular'});
    } catch {
        textFont = 'CuteFont';
        registerFont(`server/fonts/${textFont}-Regular.ttf`, { family: textFont, weight: 'regular'});
    }
    const helperCtx = createCanvas(6000, 6000).getContext('2d');
    helperCtx.font = `regular ${textSize}pt ${textFont}`;

    const textMeasure = helperCtx.measureText(text);

    const margin = textSize * 0.3;
    const width = textMeasure.width + 2 * margin;
    const height = (textMeasure.actualBoundingBoxAscent + textMeasure.actualBoundingBoxDescent) + 2 * margin;

    const canvas = createCanvas(width, height);
    const context = canvas.getContext('2d');

    if (backgroundColour !== '#transparent') {
        context.fillStyle = backgroundColour;
        context.fillRect(0, 0, width, height);
    }

    context.font = `regular ${textSize}pt ${textFont}`;
    context.fillStyle = textColour;
    context.fillText(text, margin, height - margin - textMeasure.actualBoundingBoxDescent / 2);

    return canvas.toBuffer('image/png');
}

export default async function (req, res, next) {
    const url = new URL(req.url, process.env.BASE_URL);
    const isText = url.pathname.endsWith('.json');
    const id = url.pathname.substring(1, isText ? url.pathname.length - 5 : undefined);

    if (!id.match(/^[A-Za-z0-9]{6}$/)) {
        next();
        return;
    }

    const backgroundColour = '#' + (url.searchParams.get('bg') || '76036f');
    const textColour = '#' + (url.searchParams.get('colour') || 'fcecfb');
    const textSize = Math.max(5, Math.min(parseInt(url.searchParams.get('size')) || 24, 80));
    const textFont = url.searchParams.get('font') || 'CuteFont';
    const digits = Math.max(1, Math.min(parseInt(url.searchParams.get('digits')) || 6, 24));
    const initial = parseInt(url.searchParams.get('initial')) || 0;

    const hits = initial + await fetchAndIncrementsHits(id, !!url.searchParams.has('justChecking'));

    res.setHeader('Access-Control-Allow-Origin', '*');

    if (isText) {
        res.setHeader('content-type', 'application.json');
        res.write('' + hits);
        res.end();
        return;
    }

    const text = ('' + hits).padStart(digits, '0');

    res.setHeader('content-type', 'image/png');
    res.write(await drawCounter(text, backgroundColour, textColour, textSize, textFont));
    res.end();
}
