const sqlite = require('sqlite');
const sqlite3 = require('sqlite3');

async function initDb() {
    const db = await sqlite.open({
        filename: __dirname + '/../db.sqlite',
        driver: sqlite3.Database,
    });

    await db.exec(`DROP TABLE IF EXISTS counters`);
    await db.exec(`CREATE TABLE counters (
        id TEXT NOT NULL PRIMARY KEY,
        hits INTEGER NOT NULL
    )`);
}

initDb();
